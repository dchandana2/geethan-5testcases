package com.tpo.util;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class wedDriverUtil {

    public static WebDriver driver;
    private static Map<String, Object> vars;
    public static JavascriptExecutor js;

    public static WebDriver getDriver() {
        return driver;
    }

    public static void setDriver(WebDriver driver) {
        wedDriverUtil.driver = driver;
    }

    public static void openDrowser(){

        ChromeOptions web_options = new ChromeOptions();
        web_options.addArguments("--incognito");

        driver = new ChromeDriver(web_options);
        js = (JavascriptExecutor) driver;
        vars = new HashMap<String, Object>();

        //Maximize The Window
        driver.manage().window().maximize();

        //Delete Cookies
        driver.manage().deleteAllCookies();

        //Set Timeouts
        driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
    }



}
