package com.tpo.testScripts;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import java.util.List;

import static com.tpo.businessComponent.tpoLibCommon.*;
import static com.tpo.businessComponent.tpolibHome.search_query;
import static com.tpo.util.wedDriverUtil.driver;

public class SearchResultsColumnViewSettingsTest {

    @AfterTest
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void searchResultsColumnViewSettingsTest() throws InterruptedException {

        //Get user Data From Excel
        String user_data = get_user_data();
        String[] user_data_array = user_data.split("@@@");
        String got_uname = user_data_array[0];
        String got_pwd = user_data_array[1];

        //Login To TPO
        login_to_tpo(got_uname,got_pwd);

        //Perform Search Query
        search_query("car");

        //Start testing Part
        driver.findElement(By.cssSelector(".drop-down-button-chevron")).click();
        driver.findElement(By.cssSelector(".drop-down-list-item:nth-child(1) .drop-down-list-item-link")).click();

        Thread.sleep(8000);

        //------------------

        //start Selecting Column Names
        //-----------------------



        driver.findElement(By.cssSelector(".icon-settings")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("labelTITLE")).click();
        Thread.sleep(2000);
        driver.findElement(By.cssSelector(".lnip-button-primary")).click();
        Thread.sleep(5000);

        {
            List<WebElement> elements = driver.findElements(By.cssSelector(".table2-header-TI > lnip-table-header"));
            assert(elements.size() > 0);
        }
        //------------------------


        driver.findElement(By.cssSelector(".icon-settings")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("labelCURRENT_STANDARDIZED_ASSIGNEE")).click();
        Thread.sleep(2000);
        driver.findElement(By.cssSelector(".lnip-button-primary")).click();
        Thread.sleep(5000);

        {
            List<WebElement> elements = driver.findElements(By.cssSelector(".table2-header-PACUS > lnip-table-header"));
            assert(elements.size() > 0);
        }
        Thread.sleep(2000);
        //------------------------




        driver.findElement(By.cssSelector(".icon-settings")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("labelIPC")).click();
        Thread.sleep(2000);
        driver.findElement(By.cssSelector(".lnip-button-primary")).click();
        Thread.sleep(5000);

        {
            List<WebElement> elements = driver.findElements(By.cssSelector(".table2-header-IPC > lnip-table-header"));
            assert(elements.size() > 0);
        }
        //------------------------




        driver.findElement(By.cssSelector(".icon-settings")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("labelCPC")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("labelULTIMATE_OWNER")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("labelSTANDARDIZED_ASSIGNEE")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("labelINVENTOR")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("labelAPPLICATION_NUMBER")).click();
        Thread.sleep(2000);
        driver.findElement(By.cssSelector(".lnip-button-primary")).click();
        Thread.sleep(5000);

        {
            List<WebElement> elements = driver.findElements(By.cssSelector(".table2-header-CPC > lnip-table-header"));
            assert(elements.size() > 0);
        }
        Thread.sleep(1000);
        {
            List<WebElement> elements = driver.findElements(By.cssSelector(".table2-header-UO > lnip-table-header"));
            assert(elements.size() > 0);
        }
        Thread.sleep(1000);
        {
            List<WebElement> elements = driver.findElements(By.cssSelector(".table2-header-PAS > lnip-table-header"));
            assert(elements.size() > 0);
        }
        Thread.sleep(1000);
        {
            List<WebElement> elements = driver.findElements(By.cssSelector(".table2-header-IN > lnip-table-header"));
            assert(elements.size() > 0);
        }
        Thread.sleep(1000);
        {
            List<WebElement> elements = driver.findElements(By.cssSelector(".table2-header-AN > lnip-table-header"));
            assert(elements.size() > 0);
        }
        Thread.sleep(2000);

        //------------------------


        driver.findElement(By.cssSelector(".icon-settings")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("labelAPPLICATION_FILING_DATE")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("labelPRIORITY_NUMBERS")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("labelPRIORITY_DATE")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("labelABSTRACT")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("labelFIRST_CLAIM")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("labelCLAIMS_COUNT")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("labelCLIPPED_IMAGE")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("labelUS_CLASS")).click();
        Thread.sleep(2000);
        driver.findElement(By.cssSelector(".lnip-button-primary")).click();
        Thread.sleep(5000);

        {
            List<WebElement> elements = driver.findElements(By.cssSelector(".table2-header-AD > lnip-table-header"));
            assert(elements.size() > 0);
        }
        Thread.sleep(1000);
        {
            List<WebElement> elements = driver.findElements(By.cssSelector(".table2-header-PRN > lnip-table-header"));
            assert(elements.size() > 0);
        }
        Thread.sleep(1000);
        {
            List<WebElement> elements = driver.findElements(By.cssSelector(".table2-header-PRD > lnip-table-header"));
            assert(elements.size() > 0);
        }
        Thread.sleep(1000);
        {
            List<WebElement> elements = driver.findElements(By.cssSelector(".table2-header-AB > lnip-table-header"));
            assert(elements.size() > 0);
        }
        Thread.sleep(1000);
        {
            List<WebElement> elements = driver.findElements(By.cssSelector(".table2-header-CLM1 > lnip-table-header"));
            assert(elements.size() > 0);
        }
        Thread.sleep(1000);
        {
            List<WebElement> elements = driver.findElements(By.cssSelector(".table2-header-TCC > lnip-table-header"));
            assert(elements.size() > 0);
        }
        Thread.sleep(1000);
        {
            List<WebElement> elements = driver.findElements(By.cssSelector(".table2-header-CLIPPED_IMAGE > lnip-table-header"));
            assert(elements.size() > 0);
        }
        Thread.sleep(1000);
        {
            List<WebElement> elements = driver.findElements(By.cssSelector(".table2-header-USC > lnip-table-header"));
            assert(elements.size() > 0);
        }
        //------------------------




        driver.findElement(By.cssSelector(".icon-settings")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("labelFI")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("labelF_TERMS")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("labelLOCARNO")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("labelBACKWARD_COUNT")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("labelFORWARD_COUNT")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("labelLITIGATION_OPPOSITION")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("labelPHARMA")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("labelANNOTATION")).click();
        Thread.sleep(1000);
        driver.findElement(By.cssSelector(".lnip-button-primary")).click();
        Thread.sleep(5000);

        {
            List<WebElement> elements = driver.findElements(By.cssSelector(".table2-header-FIC > lnip-table-header"));
            assert(elements.size() > 0);
        }
        Thread.sleep(1000);
        {
            List<WebElement> elements = driver.findElements(By.cssSelector(".table2-header-FTC > lnip-table-header"));
            assert(elements.size() > 0);
        }
        Thread.sleep(1000);
        {
            List<WebElement> elements = driver.findElements(By.cssSelector(".table2-header-LOC > lnip-table-header"));
            assert(elements.size() > 0);
        }
        Thread.sleep(1000);
        {
            List<WebElement> elements = driver.findElements(By.cssSelector(".table2-header-BCC > lnip-table-header"));
            assert(elements.size() > 0);
        }
        Thread.sleep(1000);
        {
            List<WebElement> elements = driver.findElements(By.cssSelector(".table2-header-FCC > lnip-table-header"));
            assert(elements.size() > 0);
        }
        Thread.sleep(1000);
        {
            List<WebElement> elements = driver.findElements(By.cssSelector(".table2-header-LIOPI > lnip-table-header"));
            assert(elements.size() > 0);
        }
        Thread.sleep(1000);
        {
            List<WebElement> elements = driver.findElements(By.cssSelector(".table2-header-PHRI > lnip-table-header"));
            assert(elements.size() > 0);
        }
        Thread.sleep(1000);
        {
            List<WebElement> elements = driver.findElements(By.cssSelector(".table2-header-ANNOTATION > lnip-table-header"));
            assert(elements.size() > 0);
        }
        Thread.sleep(1000);
        //------------------------

        //Logout From TPO
        logout_frm_tpo();

    }

}
