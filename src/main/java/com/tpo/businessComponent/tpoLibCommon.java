package com.tpo.businessComponent;

import com.tpo.util.wedDriverUtil;
import com.tpo.pages.tpoLogin;
import com.tpo.pages.tpoHome;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;

import java.io.File;
import java.io.FileInputStream;

import static com.tpo.pages.tpoHome.top_user_dropdown;
import static com.tpo.pages.tpoHome.user_dropdown_signout;


public class tpoLibCommon extends wedDriverUtil{

    public static void login_to_tpo(String get_tpo_lgn_uname , String get_tpo_lgn_pwd){

        wedDriverUtil.openDrowser();
        driver.get("http://passive.totalpatentone.com");
        driver.findElement(tpoLogin.tpo_lgn_uname).sendKeys(get_tpo_lgn_uname);
        driver.findElement(tpoLogin.tpo_lgn_pwd).sendKeys(get_tpo_lgn_pwd);
        driver.findElement(tpoLogin.tpo_lgn_sbmt_btn).click();

    }

    public static void logout_frm_tpo(){

        driver.findElement(top_user_dropdown).click();
        driver.findElement(user_dropdown_signout).click();

    }

    public static String get_user_data(){

        //Read Data From Excel File
        String u_username = null;
        String p_password = null;
        try {
            FileInputStream the_file = new FileInputStream(new File("C:\\\\Users\\\\geethanperera\\\\Downloads\\\\unm_n_pw.xlsx"));
            XSSFWorkbook workbook = new XSSFWorkbook(the_file);
            XSSFSheet sheet = workbook.getSheetAt(0);
            Row first_row = sheet.getRow(0);
            //String u_name = first_row.getCell(0);
            u_username = String.valueOf(sheet.getRow(0).getCell(0));
            p_password = String.valueOf(sheet.getRow(0).getCell(1));

        } catch (Exception e) {
            e.printStackTrace();
        }

        //End Reading Excel File

        String return_val =  u_username+"@@@"+p_password;
        return return_val;

    }



}
