package com.tpo.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static com.tpo.util.wedDriverUtil.driver;

public class tpoHome {

    public static By search_txt_area =  By.cssSelector(".object-search-text-editor");
    public static WebElement srch_bx_element = driver.findElement(search_txt_area);

    public static By top_user_dropdown =  By.cssSelector(".drop-down-wrapper > .icon-user-preferences");
    public static By user_dropdown_signout =  By.cssSelector(".drop-down-list-item:nth-child(5) .drop-down-list-item-link");

    public static By search_btn_submit =  By.cssSelector(".object-search-search-btn .ng-star-inserted");


}
